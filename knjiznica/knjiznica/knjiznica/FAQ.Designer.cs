﻿namespace knjiznica
{
    partial class FAQ
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboVprasanja = new System.Windows.Forms.ComboBox();
            this.labelOdg = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // comboVprasanja
            // 
            this.comboVprasanja.FormattingEnabled = true;
            this.comboVprasanja.Location = new System.Drawing.Point(233, 133);
            this.comboVprasanja.Name = "comboVprasanja";
            this.comboVprasanja.Size = new System.Drawing.Size(314, 24);
            this.comboVprasanja.TabIndex = 0;
            // 
            // labelOdg
            // 
            this.labelOdg.AutoSize = true;
            this.labelOdg.Location = new System.Drawing.Point(326, 173);
            this.labelOdg.Name = "labelOdg";
            this.labelOdg.Size = new System.Drawing.Size(75, 17);
            this.labelOdg.TabIndex = 1;
            this.labelOdg.Text = "Odgovor...";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(326, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Izberi vprašanje:";
            // 
            // FAQ
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.ControlBox = false;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelOdg);
            this.Controls.Add(this.comboVprasanja);
            this.Name = "FAQ";
            this.Text = "FAQ";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboVprasanja;
        private System.Windows.Forms.Label labelOdg;
        private System.Windows.Forms.Label label2;
    }
}