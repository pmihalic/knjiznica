﻿namespace knjiznica
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.homeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iskanjeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.naprednoIskanjeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mojRačunToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mojeIzposojeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fAQToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.odjavaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Ibarra Real Nova SemiBold", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.homeToolStripMenuItem,
            this.iskanjeToolStripMenuItem,
            this.naprednoIskanjeToolStripMenuItem,
            this.mojRačunToolStripMenuItem,
            this.mojeIzposojeToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.fAQToolStripMenuItem,
            this.odjavaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1182, 37);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // homeToolStripMenuItem
            // 
            this.homeToolStripMenuItem.Name = "homeToolStripMenuItem";
            this.homeToolStripMenuItem.Size = new System.Drawing.Size(75, 33);
            this.homeToolStripMenuItem.Text = "Home";
            this.homeToolStripMenuItem.Click += new System.EventHandler(this.homeToolStripMenuItem_Click);
            // 
            // iskanjeToolStripMenuItem
            // 
            this.iskanjeToolStripMenuItem.Name = "iskanjeToolStripMenuItem";
            this.iskanjeToolStripMenuItem.Size = new System.Drawing.Size(84, 33);
            this.iskanjeToolStripMenuItem.Text = "Iskanje";
            this.iskanjeToolStripMenuItem.Click += new System.EventHandler(this.iskanjeToolStripMenuItem_Click);
            // 
            // naprednoIskanjeToolStripMenuItem
            // 
            this.naprednoIskanjeToolStripMenuItem.Name = "naprednoIskanjeToolStripMenuItem";
            this.naprednoIskanjeToolStripMenuItem.Size = new System.Drawing.Size(169, 33);
            this.naprednoIskanjeToolStripMenuItem.Text = "Napredno iskanje";
            this.naprednoIskanjeToolStripMenuItem.Click += new System.EventHandler(this.naprednoIskanjeToolStripMenuItem_Click);
            // 
            // mojRačunToolStripMenuItem
            // 
            this.mojRačunToolStripMenuItem.Name = "mojRačunToolStripMenuItem";
            this.mojRačunToolStripMenuItem.Size = new System.Drawing.Size(112, 33);
            this.mojRačunToolStripMenuItem.Text = "Moj račun";
            this.mojRačunToolStripMenuItem.Click += new System.EventHandler(this.mojRačunToolStripMenuItem_Click);
            // 
            // mojeIzposojeToolStripMenuItem
            // 
            this.mojeIzposojeToolStripMenuItem.Name = "mojeIzposojeToolStripMenuItem";
            this.mojeIzposojeToolStripMenuItem.Size = new System.Drawing.Size(137, 33);
            this.mojeIzposojeToolStripMenuItem.Text = "Moje izposoje";
            this.mojeIzposojeToolStripMenuItem.Click += new System.EventHandler(this.mojeIzposojeToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(65, 33);
            this.helpToolStripMenuItem.Text = "Help";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // fAQToolStripMenuItem
            // 
            this.fAQToolStripMenuItem.Name = "fAQToolStripMenuItem";
            this.fAQToolStripMenuItem.Size = new System.Drawing.Size(65, 33);
            this.fAQToolStripMenuItem.Text = "FAQ";
            this.fAQToolStripMenuItem.Click += new System.EventHandler(this.fAQToolStripMenuItem_Click);
            // 
            // odjavaToolStripMenuItem
            // 
            this.odjavaToolStripMenuItem.Name = "odjavaToolStripMenuItem";
            this.odjavaToolStripMenuItem.Size = new System.Drawing.Size(82, 33);
            this.odjavaToolStripMenuItem.Text = "Odjava";
            this.odjavaToolStripMenuItem.Click += new System.EventHandler(this.odjavaToolStripMenuItem_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1182, 553);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Main";
            this.Text = "Kolibri";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Main_FormClosed);
            this.Load += new System.EventHandler(this.Main_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem homeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iskanjeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem naprednoIskanjeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mojRačunToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mojeIzposojeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fAQToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem odjavaToolStripMenuItem;
    }
}

