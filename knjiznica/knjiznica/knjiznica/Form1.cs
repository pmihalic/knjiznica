﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace knjiznica
{
    public partial class Main : Form
    {
        public Main()
        {

            InitializeComponent();
           // this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            //this.WindowState = System.Windows.Forms.FormWindowState.Maximized;

        }

        private void homeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool isOpen = false;
            foreach (Form f in Application.OpenForms) {
                if (f.Text == "Home") {
                    isOpen = true;
                    f.Focus();
                    break;
                }          
            }

            if (isOpen == false) {
                Home h = new Home();
                h.MdiParent = this;
                h.Show();
               // h.WindowState = FormWindowState.Maximized;
            }
        }

        private void homeToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
           
        }

        private void Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void iskanjeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool isOpen = false;
            
            foreach (Form f in Application.OpenForms)
            {
                if (f.Text == "Iskanje")
                {
                    isOpen = true;
                    f.Focus();
                    break;
                }
            }

            

            if (isOpen == false)
            {
                Iskanje h = new Iskanje();
                h.MdiParent = this;
                h.Show();
                h.WindowState = FormWindowState.Maximized;
            }
        }

        private void naprednoIskanjeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool isOpen = false;
            foreach (Form f in Application.OpenForms)
            {
                if (f.Text == "NaprednoIskanje")
                {
                    isOpen = true;
                    f.Focus();
                    break;
                }
            }

            if (isOpen == false)
            {
                NaprednoIskanje h = new NaprednoIskanje();
                h.MdiParent = this;
                h.Show();
                h.WindowState = FormWindowState.Maximized;
            }
        }

        private void mojRačunToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool isOpen = false;
            foreach (Form f in Application.OpenForms)
            {
                if (f.Text == "MojRacun")
                {
                    isOpen = true;
                    f.Focus();
                    break;
                }
            }

            if (isOpen == false)
            {
                MojRacun h = new MojRacun();
                h.MdiParent = this;
                h.Show();
                h.WindowState = FormWindowState.Maximized;
            }
        }

        private void mojeIzposojeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool isOpen = false;
            foreach (Form f in Application.OpenForms)
            {
                if (f.Text == "MojeIzposoje")
                {
                    isOpen = true;
                    f.Focus();
                    break;
                }
            }

            if (isOpen == false)
            {
                MojeIzposoje h = new MojeIzposoje();
                h.MdiParent = this;
                h.Show();
                h.WindowState = FormWindowState.Maximized;
            }
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool isOpen = false;
            foreach (Form f in Application.OpenForms)
            {
                if (f.Text == "Help")
                {
                    isOpen = true;
                    f.Focus();
                    break;
                }
            }

            if (isOpen == false)
            {
                Help h = new Help();
                h.MdiParent = this;
                h.Show();
                h.WindowState = FormWindowState.Maximized;
            }

        }

        private void fAQToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool isOpen = false;
            foreach (Form f in Application.OpenForms)
            {
                if (f.Text == "FAQ")
                {
                    isOpen = true;
                    f.Focus();
                    break;
                }
            }

            if (isOpen == false)
            {
                FAQ h = new FAQ();
                h.MdiParent = this;
                h.Show();
                h.WindowState = FormWindowState.Maximized;
            }
        }

        private void odjavaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            bool isOpen = false;
            foreach (Form f in Application.OpenForms)
            {
                if (f.Text == "Home")
                {
                    isOpen = true;
                    f.Focus();
                    break;
                }
            }

            if (isOpen == false)
            {
                Home h = new Home();
                h.MdiParent = this;
                h.Show();
                h.WindowState = FormWindowState.Maximized;
            }
        }
    }
}
