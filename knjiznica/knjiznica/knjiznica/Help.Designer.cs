﻿namespace knjiznica
{
    partial class Help
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textHelp = new System.Windows.Forms.TextBox();
            this.buttonPoslji = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonOdustani = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textHelp
            // 
            this.textHelp.Location = new System.Drawing.Point(157, 111);
            this.textHelp.Multiline = true;
            this.textHelp.Name = "textHelp";
            this.textHelp.Size = new System.Drawing.Size(473, 242);
            this.textHelp.TabIndex = 0;
            // 
            // buttonPoslji
            // 
            this.buttonPoslji.Location = new System.Drawing.Point(546, 393);
            this.buttonPoslji.Name = "buttonPoslji";
            this.buttonPoslji.Size = new System.Drawing.Size(132, 45);
            this.buttonPoslji.TabIndex = 1;
            this.buttonPoslji.Text = "Pošlji";
            this.buttonPoslji.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(154, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(320, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Vnesite vaše vprašanje v obrazec in klknite pošlji.";
            // 
            // buttonOdustani
            // 
            this.buttonOdustani.Location = new System.Drawing.Point(696, 397);
            this.buttonOdustani.Name = "buttonOdustani";
            this.buttonOdustani.Size = new System.Drawing.Size(77, 40);
            this.buttonOdustani.TabIndex = 4;
            this.buttonOdustani.Text = "Odustani";
            this.buttonOdustani.UseVisualStyleBackColor = true;
            // 
            // Help
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.ControlBox = false;
            this.Controls.Add(this.buttonOdustani);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonPoslji);
            this.Controls.Add(this.textHelp);
            this.Name = "Help";
            this.Text = "Help";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textHelp;
        private System.Windows.Forms.Button buttonPoslji;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonOdustani;
    }
}