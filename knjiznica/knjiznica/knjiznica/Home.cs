﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace knjiznica
{
    public partial class Home : Form
    {
        public Home()
        {
            
            InitializeComponent();
            label1.Text = "Dobrodošel v Kolibri, " + Varijable.uporabnik.ToString() + " !";
            label1.Left = (this.ClientSize.Width - label1.Width) / 2;
            label1.Top = 10;
            pictureBox1.Width = this.ClientSize.Width;
            //button1.Left= (this.ClientSize.Width)/2+button1.Width/2;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Iskanje m = new Iskanje();
            m.MdiParent = this.MdiParent;
            m.Show();
            m.WindowState = FormWindowState.Maximized;
        }
    }
}
