﻿namespace knjiznica
{
    partial class InfoAutor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelIme = new System.Windows.Forms.Label();
            this.labelDatum = new System.Windows.Forms.Label();
            this.labelPriimek = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.listDela = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(143, 108);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ime:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(143, 137);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Priimek:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(143, 164);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Datum rojstva:";
            // 
            // labelIme
            // 
            this.labelIme.AutoSize = true;
            this.labelIme.Location = new System.Drawing.Point(300, 108);
            this.labelIme.Name = "labelIme";
            this.labelIme.Size = new System.Drawing.Size(46, 17);
            this.labelIme.TabIndex = 3;
            this.labelIme.Text = "label4";
            // 
            // labelDatum
            // 
            this.labelDatum.AutoSize = true;
            this.labelDatum.Location = new System.Drawing.Point(300, 164);
            this.labelDatum.Name = "labelDatum";
            this.labelDatum.Size = new System.Drawing.Size(46, 17);
            this.labelDatum.TabIndex = 4;
            this.labelDatum.Text = "label5";
            // 
            // labelPriimek
            // 
            this.labelPriimek.AutoSize = true;
            this.labelPriimek.Location = new System.Drawing.Point(300, 137);
            this.labelPriimek.Name = "labelPriimek";
            this.labelPriimek.Size = new System.Drawing.Size(46, 17);
            this.labelPriimek.TabIndex = 5;
            this.labelPriimek.Text = "label6";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(143, 204);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Seznam dela:";
            // 
            // listDela
            // 
            this.listDela.FormattingEnabled = true;
            this.listDela.ItemHeight = 16;
            this.listDela.Location = new System.Drawing.Point(303, 204);
            this.listDela.Name = "listDela";
            this.listDela.Size = new System.Drawing.Size(368, 212);
            this.listDela.TabIndex = 7;
            // 
            // InfoAutor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.listDela);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.labelPriimek);
            this.Controls.Add(this.labelDatum);
            this.Controls.Add(this.labelIme);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "InfoAutor";
            this.Text = "InfoAutor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelIme;
        private System.Windows.Forms.Label labelDatum;
        private System.Windows.Forms.Label labelPriimek;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox listDela;
    }
}