﻿namespace knjiznica
{
    partial class InfoKnjigacs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelNaziv = new System.Windows.Forms.Label();
            this.labelAutor = new System.Windows.Forms.Label();
            this.labelISBN = new System.Windows.Forms.Label();
            this.labelZanr = new System.Windows.Forms.Label();
            this.labelStranice = new System.Windows.Forms.Label();
            this.labelJezik = new System.Windows.Forms.Label();
            this.labelIzdavatelj = new System.Windows.Forms.Label();
            this.labelLeto = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(118, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Naziv:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(118, 105);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Autor:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(118, 132);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "ISBN:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(118, 158);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Žanr";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(118, 185);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Število stranic:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(119, 213);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "Jezik:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(119, 241);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 17);
            this.label7.TabIndex = 6;
            this.label7.Text = "Leto izdaje:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(119, 270);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 17);
            this.label8.TabIndex = 7;
            this.label8.Text = "Izadavatelj:";
            // 
            // labelNaziv
            // 
            this.labelNaziv.AutoSize = true;
            this.labelNaziv.Location = new System.Drawing.Point(256, 79);
            this.labelNaziv.Name = "labelNaziv";
            this.labelNaziv.Size = new System.Drawing.Size(73, 17);
            this.labelNaziv.TabIndex = 8;
            this.labelNaziv.Text = "labelNaziv";
            // 
            // labelAutor
            // 
            this.labelAutor.AutoSize = true;
            this.labelAutor.Location = new System.Drawing.Point(256, 105);
            this.labelAutor.Name = "labelAutor";
            this.labelAutor.Size = new System.Drawing.Size(54, 17);
            this.labelAutor.TabIndex = 9;
            this.labelAutor.Text = "label10";
            // 
            // labelISBN
            // 
            this.labelISBN.AutoSize = true;
            this.labelISBN.Location = new System.Drawing.Point(256, 132);
            this.labelISBN.Name = "labelISBN";
            this.labelISBN.Size = new System.Drawing.Size(54, 17);
            this.labelISBN.TabIndex = 10;
            this.labelISBN.Text = "label11";
            // 
            // labelZanr
            // 
            this.labelZanr.AutoSize = true;
            this.labelZanr.Location = new System.Drawing.Point(256, 158);
            this.labelZanr.Name = "labelZanr";
            this.labelZanr.Size = new System.Drawing.Size(54, 17);
            this.labelZanr.TabIndex = 11;
            this.labelZanr.Text = "label12";
            // 
            // labelStranice
            // 
            this.labelStranice.AutoSize = true;
            this.labelStranice.Location = new System.Drawing.Point(256, 185);
            this.labelStranice.Name = "labelStranice";
            this.labelStranice.Size = new System.Drawing.Size(54, 17);
            this.labelStranice.TabIndex = 12;
            this.labelStranice.Text = "label13";
            // 
            // labelJezik
            // 
            this.labelJezik.AutoSize = true;
            this.labelJezik.Location = new System.Drawing.Point(256, 213);
            this.labelJezik.Name = "labelJezik";
            this.labelJezik.Size = new System.Drawing.Size(54, 17);
            this.labelJezik.TabIndex = 13;
            this.labelJezik.Text = "label14";
            // 
            // labelIzdavatelj
            // 
            this.labelIzdavatelj.AutoSize = true;
            this.labelIzdavatelj.Location = new System.Drawing.Point(256, 270);
            this.labelIzdavatelj.Name = "labelIzdavatelj";
            this.labelIzdavatelj.Size = new System.Drawing.Size(54, 17);
            this.labelIzdavatelj.TabIndex = 14;
            this.labelIzdavatelj.Text = "label15";
            // 
            // labelLeto
            // 
            this.labelLeto.AutoSize = true;
            this.labelLeto.Location = new System.Drawing.Point(256, 241);
            this.labelLeto.Name = "labelLeto";
            this.labelLeto.Size = new System.Drawing.Size(54, 17);
            this.labelLeto.TabIndex = 15;
            this.labelLeto.Text = "label16";
            // 
            // InfoKnjigacs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.labelLeto);
            this.Controls.Add(this.labelIzdavatelj);
            this.Controls.Add(this.labelJezik);
            this.Controls.Add(this.labelStranice);
            this.Controls.Add(this.labelZanr);
            this.Controls.Add(this.labelISBN);
            this.Controls.Add(this.labelAutor);
            this.Controls.Add(this.labelNaziv);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "InfoKnjigacs";
            this.Text = "InfoKnjigacs";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelNaziv;
        private System.Windows.Forms.Label labelAutor;
        private System.Windows.Forms.Label labelISBN;
        private System.Windows.Forms.Label labelZanr;
        private System.Windows.Forms.Label labelStranice;
        private System.Windows.Forms.Label labelJezik;
        private System.Windows.Forms.Label labelIzdavatelj;
        private System.Windows.Forms.Label labelLeto;
    }
}