﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.Script.Serialization;
using Newtonsoft.Json;


namespace knjiznica
{
    public partial class Iskanje : Form
    {
        public Iskanje()
        {
            InitializeComponent();
        }

        List<Knjiga> vseKnjige;

        private void Iskanje_Load(object sender, EventArgs e)
        {
            using (var client = new WebClient())
            {
                var values = new NameValueCollection();
                values["type"] = "allBooks";

                var response = client.UploadValues("https://nikolag.ddns.net/ian/index.php", values);
                //var response = client.UploadValues("https://nikolag.ddns.net/ian/getAllBooksNew.php", values);

                var responseString = Encoding.Default.GetString(response);
               // label1.Text = responseString;



                List<Knjiga> knjige = JsonConvert.DeserializeObject<List<Knjiga>>(responseString);
                vseKnjige = knjige.ToList();

                foreach (var knjiga in knjige) {
                    var row = new string[] { knjiga.Author, knjiga.Name, knjiga.Number.ToString() };
                    var lvi = new ListViewItem(row);

                    lvi.Tag = knjiga;

                    listKnjige.Items.Add(lvi);

                }
                //label2.Text = knjige[0].Number.ToString();
                

            }
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            bool found = false;
            listRez.Items.Clear();
            listRez.Refresh();
            string isci = textSearch.Text;
            foreach (var knjiga in vseKnjige)
            {
                if (knjiga.Name == isci)
                {
                    found = true;
                    var row = new string[] { knjiga.Name, knjiga.Author, knjiga.Number.ToString() };
                    var lvi = new ListViewItem(row);

                    lvi.Tag = knjiga;
                    listRez.Items.Add(lvi);

                }
               
            }
            if (found == false)
            {
                 
                    MessageBox.Show("Ne obstaja ta knjiga!");
                    textSearch.Text = "";
                
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string user = Varijable.uporabnik.ToString();
            string book = listRez.SelectedItems[0].Text;
            //label1.Text=user + " " + book;
            using (var client = new WebClient())
            {
                var values = new NameValueCollection();
                values["type"] = "rent";
                values["email"] = user;
                values["book"] = book;
                var response = client.UploadValues("https://nikolag.ddns.net/ian/index.php", values);

                var responseString = Encoding.Default.GetString(response);
                //label1.Text = responseString;

                if (responseString == "1")
                {
                    MessageBox.Show("Uspešna izposoja!!");

                }
                else
                {
                    MessageBox.Show("NONOO!!");
                }
            }
        }
    }
}
