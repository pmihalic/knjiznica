﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace knjiznica
{
    public class Knjiga
    {
        public string ISBN { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public DateTime Date { get; set; }
        public int Pages { get; set; }
        public string Author { get; set; }
        public string Language { get; set; }
        public string Publisher { get; set; }

        public string Genre { get; set; }

        public int Number { get; set; }

        public int Daysleft { get; set; }
    }
}
