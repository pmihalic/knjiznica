﻿namespace knjiznica
{
    partial class MojRacun
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textIme = new System.Windows.Forms.TextBox();
            this.textPriimek = new System.Windows.Forms.TextBox();
            this.textUporabnisko = new System.Windows.Forms.TextBox();
            this.textMail = new System.Windows.Forms.TextBox();
            this.buttonUredi = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(57, 82);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ime";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(57, 112);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Priimek";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(57, 137);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Uporabnisko ime";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(57, 164);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "e-mail";
            // 
            // textIme
            // 
            this.textIme.Location = new System.Drawing.Point(184, 79);
            this.textIme.Name = "textIme";
            this.textIme.Size = new System.Drawing.Size(177, 22);
            this.textIme.TabIndex = 6;
            // 
            // textPriimek
            // 
            this.textPriimek.Location = new System.Drawing.Point(184, 112);
            this.textPriimek.Name = "textPriimek";
            this.textPriimek.Size = new System.Drawing.Size(180, 22);
            this.textPriimek.TabIndex = 7;
            // 
            // textUporabnisko
            // 
            this.textUporabnisko.Location = new System.Drawing.Point(184, 140);
            this.textUporabnisko.Name = "textUporabnisko";
            this.textUporabnisko.Size = new System.Drawing.Size(180, 22);
            this.textUporabnisko.TabIndex = 8;
            // 
            // textMail
            // 
            this.textMail.Location = new System.Drawing.Point(184, 168);
            this.textMail.Name = "textMail";
            this.textMail.Size = new System.Drawing.Size(177, 22);
            this.textMail.TabIndex = 9;
            // 
            // buttonUredi
            // 
            this.buttonUredi.Location = new System.Drawing.Point(409, 102);
            this.buttonUredi.Name = "buttonUredi";
            this.buttonUredi.Size = new System.Drawing.Size(121, 42);
            this.buttonUredi.TabIndex = 12;
            this.buttonUredi.Text = "Uredi";
            this.buttonUredi.UseVisualStyleBackColor = true;
            // 
            // MojRacun
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonUredi);
            this.Controls.Add(this.textMail);
            this.Controls.Add(this.textUporabnisko);
            this.Controls.Add(this.textPriimek);
            this.Controls.Add(this.textIme);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "MojRacun";
            this.Text = "MojRacun";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textIme;
        private System.Windows.Forms.TextBox textPriimek;
        private System.Windows.Forms.TextBox textUporabnisko;
        private System.Windows.Forms.TextBox textMail;
        private System.Windows.Forms.Button buttonUredi;
    }
}