﻿namespace knjiznica
{
    partial class MojeIzposoje
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MojeIzposoje));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.listTrenutne = new System.Windows.Forms.ListView();
            this.avtor = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.name = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.number = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttonReturn = new System.Windows.Forms.Button();
            this.listVse = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ibarra Real Nova", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(98, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(173, 30);
            this.label1.TabIndex = 0;
            this.label1.Text = "Trenutne izposoje:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ibarra Real Nova", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(745, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 30);
            this.label2.TabIndex = 1;
            this.label2.Text = "Vse izposoje:";
            // 
            // listTrenutne
            // 
            this.listTrenutne.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listTrenutne.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.avtor,
            this.name,
            this.number});
            this.listTrenutne.Font = new System.Drawing.Font("Ibarra Real Nova", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listTrenutne.FullRowSelect = true;
            this.listTrenutne.HideSelection = false;
            this.listTrenutne.Location = new System.Drawing.Point(103, 87);
            this.listTrenutne.Name = "listTrenutne";
            this.listTrenutne.Size = new System.Drawing.Size(522, 428);
            this.listTrenutne.TabIndex = 5;
            this.listTrenutne.UseCompatibleStateImageBehavior = false;
            this.listTrenutne.View = System.Windows.Forms.View.Details;
            // 
            // avtor
            // 
            this.avtor.Text = "Avtor";
            this.avtor.Width = 166;
            // 
            // name
            // 
            this.name.Text = "Naslov";
            this.name.Width = 191;
            // 
            // number
            // 
            this.number.Tag = "";
            this.number.Text = "Preostalo";
            this.number.Width = 90;
            // 
            // buttonReturn
            // 
            this.buttonReturn.Font = new System.Drawing.Font("Ibarra Real Nova", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonReturn.Location = new System.Drawing.Point(370, 22);
            this.buttonReturn.Name = "buttonReturn";
            this.buttonReturn.Size = new System.Drawing.Size(242, 41);
            this.buttonReturn.TabIndex = 7;
            this.buttonReturn.Text = "Vrni";
            this.buttonReturn.UseVisualStyleBackColor = true;
            this.buttonReturn.Click += new System.EventHandler(this.buttonReturn_Click);
            // 
            // listVse
            // 
            this.listVse.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listVse.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.listVse.Font = new System.Drawing.Font("Ibarra Real Nova", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listVse.FullRowSelect = true;
            this.listVse.HideSelection = false;
            this.listVse.Location = new System.Drawing.Point(750, 87);
            this.listVse.Name = "listVse";
            this.listVse.Size = new System.Drawing.Size(386, 428);
            this.listVse.TabIndex = 8;
            this.listVse.UseCompatibleStateImageBehavior = false;
            this.listVse.View = System.Windows.Forms.View.Details;
            this.listVse.SelectedIndexChanged += new System.EventHandler(this.listVse_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Avtor";
            this.columnHeader1.Width = 185;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Naslov";
            this.columnHeader2.Width = 150;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1182, 555);
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(-3, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(1185, 62);
            this.label3.TabIndex = 10;
            // 
            // MojeIzposoje
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1182, 553);
            this.ControlBox = false;
            this.Controls.Add(this.listVse);
            this.Controls.Add(this.buttonReturn);
            this.Controls.Add(this.listTrenutne);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pictureBox1);
            this.Name = "MojeIzposoje";
            this.Text = "MojeIzposoje";
            this.Shown += new System.EventHandler(this.MojeIzposoje_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListView listTrenutne;
        private System.Windows.Forms.ColumnHeader avtor;
        private System.Windows.Forms.ColumnHeader name;
        private System.Windows.Forms.ColumnHeader number;
        private System.Windows.Forms.Button buttonReturn;
        private System.Windows.Forms.ListView listVse;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label3;
    }
}