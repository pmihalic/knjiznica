﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
namespace knjiznica
{
    public partial class MojeIzposoje : Form
    {
        public MojeIzposoje()
        {
            InitializeComponent();
        }

        List<Knjiga> izposojeneKnjige;
        private void MojeIzposoje_Load(object sender, EventArgs e) {

        }

        private void buttonReturn_Click(object sender, EventArgs e)
        {
            string user = Varijable.uporabnik.ToString();
            ListViewItem item = listTrenutne.SelectedItems[0];
            string book = item.SubItems[1].Text;
            //label4.Text = user + " " + book;
            using (var client = new WebClient())
            {
                var values = new NameValueCollection();
                values["type"] = "returnBook";
                values["email"] = user;
                values["book"] = book;
                var response = client.UploadValues("https://nikolag.ddns.net/ian/index.php", values);

                var responseString = Encoding.Default.GetString(response);
                //label1.Text = responseString;

                if (responseString == "1")
                {
                    MessageBox.Show("Uspešno ste vrnuli knjigo!!");
                    listTrenutne.Items.Remove(item);
                    listVse.Items.Add(item);
                }
                else
                {
                    MessageBox.Show("Neuspešno vračanje!!");
                }
            }
        }

        private void listVse_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void MojeIzposoje_Shown(object sender, EventArgs e)
        {
            using (var client = new WebClient())
            {
                var values = new NameValueCollection();
                values["type"] = "myBooks";
                values["email"] = Varijable.uporabnik.ToString();

                var response = client.UploadValues("https://nikolag.ddns.net/ian/index.php", values);


                var responseString = Encoding.Default.GetString(response);
                //label1.Text = responseString;


                listVse.Items.Clear();
                listVse.Refresh();
                List<Knjiga> knjige = JsonConvert.DeserializeObject<List<Knjiga>>(responseString);
                izposojeneKnjige = knjige.ToList();

                foreach (var knjiga in knjige)
                {
                    var row = new string[] { knjiga.Author, knjiga.Name, knjiga.Daysleft.ToString() };
                    var lvi = new ListViewItem(row);

                    lvi.Tag = knjiga;
                    if (knjiga.Daysleft < 1)
                    {
                        listVse.Items.Add(lvi);
                    }
                    else
                    {
                        listTrenutne.Items.Add(lvi);
                    }


                }
            }
        }
    }
}
