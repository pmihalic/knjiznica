﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace knjiznica
{
    public partial class NaprednoIskanje : Form
    {
        public NaprednoIskanje()
        {
            InitializeComponent();
            comboKategorija.SelectedIndex = comboKategorija.FindStringExact("Avtor");

        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            
            string keyword = textKeyword.Text;
            string selected = comboKategorija.SelectedItem.ToString();
            string typeOfSearch="byAuthorName";

            if (selected == "Avtor")
            {
                typeOfSearch = "byAuthorName";
            }
            else if (selected == "Leto izdaje")
            {
                typeOfSearch = "yearOfRelease";

            }
            else if (selected == "ISBN")
            {
                typeOfSearch = "byISBN";
            }
            else if (selected == "Zanr")
            {
                typeOfSearch = "byGenre";
            }
            //label3.Text = selected;

            using (var client = new WebClient())
            {
                var values = new NameValueCollection();
                values["type"] = typeOfSearch;
                values["variable"] = keyword;

                var response = client.UploadValues("https://nikolag.ddns.net/ian/index.php", values);

                var responseString = Encoding.Default.GetString(response);
                //label3.Text = responseString;

                if (responseString =="0")
                {
                    MessageBox.Show("Nema rezultata! ");
                }
                else
                {
                    List<Knjiga> knjige = JsonConvert.DeserializeObject<List<Knjiga>>(responseString);

                    foreach (var knjiga in knjige)
                    {
                        var row = new string[] { knjiga.Author, knjiga.Name, knjiga.ISBN, knjiga.Date.ToShortDateString() };
                        var lvi = new ListViewItem(row);

                        lvi.Tag = knjiga;

                        listRez.Items.Add(lvi);

                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string user = Varijable.uporabnik.ToString();
            ListViewItem item = listRez.SelectedItems[0];
            string book = item.SubItems[1].Text;
            //label4.Text = user + " " + book;
            using (var client = new WebClient())
            {
                var values = new NameValueCollection();
                values["type"] = "rent";
                values["email"] = user;
                values["book"] = book;
                var response = client.UploadValues("https://nikolag.ddns.net/ian/index.php", values);

                var responseString = Encoding.Default.GetString(response);
                //label1.Text = responseString;

                if (responseString == "1")
                {
                    MessageBox.Show("Uspešna izposoja!!");

                }
                else
                {
                    MessageBox.Show("Neuspešna izposoja!!");
                }
            }
        }
    }
}
