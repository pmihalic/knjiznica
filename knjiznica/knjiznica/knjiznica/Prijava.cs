﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Diagnostics;
using System.Collections.Specialized;

namespace knjiznica
{
    public partial class Prijava : Form
    {

        public Prijava()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Main m = new Main();   
            m.Show();         
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Main m = new Main();
            m.Show();
        }

        private void buttonRegistracija_Click(object sender, EventArgs e)
        {
            this.Hide();
            Registracija r = new Registracija();
            r.Show();
        }

        private void buttonPozabil_Click(object sender, EventArgs e)
        {
            this.Hide();
            PozabljenoGeslo pg = new PozabljenoGeslo();
            pg.Show();
        }

        private void buttonPrijava_Click(object sender, EventArgs e)
        {
            /*this.Hide();
            Main m = new Main();
            m.Show();*/

      
            string geslo = textGeslo.Text;
            
            string mail = textMail.Text;
            Varijable.uporabnik = textMail.Text.ToString();

            using (var client = new WebClient())
            {
                var values = new NameValueCollection();
                values["type"] = "login";
                values["password"] = geslo;
                values["email"] = mail;
                var response = client.UploadValues("https://nikolag.ddns.net/ian/index.php", values);

                var responseString = Encoding.Default.GetString(response);
                //label3.Text = responseString;

                if (responseString == "1")
                {
                    this.Hide();
                    Main m = new Main();
                    m.Show();
                    
                }
                else {
                    MessageBox.Show("Ne obstaja uporabnik s tistim e-mailom / geslom!!");
                    textGeslo.Text = "";
                    textMail.Text = "";
                    
                }
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
